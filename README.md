# WidgetCo Omnibus App
This is an app that contains ITOps, Security, Sales, and Marketing data for the WidgetCo online store.

## Prereqs
Create the following indexes:
* web
* vendor_sales

## Installation
Install this app on the Splunk Search Head